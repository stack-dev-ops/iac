#!/bin/sh
# Variable declaration
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White
IP=$(hostname -I | awk '{print $2}')
###################################################################################################################
echo "${Blue} [1]: install utils & ansible ${Blue} " $IP
apt-get update -qq >/dev/null
apt-get upgrade -y  
sed -i "s%{GPG_EXE}\")' --%{GPG_EXE}\")' --batch --%g" /usr/bin/apt-key
apt-get install -qq -y git sshpass wget ansible gnupg2 curl >/dev/null
echo "${Blue} \n utils & ansible installed successfully\n\n ${Blue}" $IP
###################################################################################################################
echo "${Blue} [2]: install java && jenkins ${Blue}" $IP
wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key >/dev/null | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
apt-get update -qq >/dev/null
apt-get install -qq -y default-jre jenkins >/dev/null
systemctl enable jenkins
systemctl start jenkins
passwordJenkins="$(sudo cat /var/lib/jenkins/secrets/initialAdminPassword)"
echo ${Purple} $passwordJenkins ${Purple};
###################################################################################################################
echo "${Blue} [3]: ansible custom ${Blue}" $IP
sed -i 's/.*pipelining.*/pipelining = True/' /etc/ansible/ansible.cfg
sed -i 's/.*allow_world_readable_tmpfiles.*/allow_world_readable_tmpfiles = True/' /etc/ansible/ansible.cfg
echo " ${Blue} \n jenkins installed successfully\n\n ${Blue}" $IP
######################################################################################################################
echo "${Blue} [4]: install docker & docker-composer ${Blue}" $IP
COMPOSE_VERSION=$(curl -s https://api.github.com/repos/docker/compose/releases/latest | grep 'tag_name' | cut -d\" -f4)
curl -fsSL https://get.docker.com | sh; >/dev/null
usermod -aG docker jenkins 
curl -sL "https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose 
echo "${Blue} \n Docker && Docker Compose installed successfully\n\n ${Blue}"
#########################################################################################################################
echo "${Cyan} END - install ${Cyan}"


